<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $primaryKey = 'product_id';
    protected $fillable = ['name', 'description', 'category_id'];
    
    public function category()
    {
        return $this->belongsTo("App\Category", "category_id");
    }
}
