<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;

class CategoryController extends Controller
{
   public function index()
   {
       $categories = Category::all();
       return response()->json(['data' =>  $categories], 200);
   }
   
   public function create(Request $request)
   {
       $category = new Category([
           'name' => $request->get('name'),
           'description' => $request->get('description'),
       ]);
       
       $category->save();
       
       return response()->json(['data' => $category], 200);
   }
   
   public function getById($id)
   {
       return Category::where('category_id', $id)->firstOrFail();
   }
   
   public function update(Request $request, $id)
   {
       $category = Category::find($id);
       
       $category->name = $request->get('name');
       $category->description = $request->get('description');
       
       $category->save();
       
       return response()->json(['data' => $category], 200);
   }
   
   public function delete($id)
   {
       $category = Category::find($id);
       $category->delete();
       
       return response()->json('Category successfully deleted', 200);
   }
}
