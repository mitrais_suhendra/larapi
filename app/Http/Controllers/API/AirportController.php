<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Airports;

class AirportController extends Controller
{
    public function index() 
    {
        $airports = Airports::all();
        return response()->json(['data' =>  $airports], 200);
    }
}
