<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// login and register
Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');

Route::group(['middleware' => 'auth:api'], function() {
    Route::post('details', 'API\UserController@details');
    Route::get('categories', 'API\CategoryController@index');
    Route::post('categories/create', 'API\CategoryController@create');
    Route::get('category/{id}', 'API\CategoryController@getById');
    Route::put('category/{id}', 'API\CategoryController@update');
    Route::delete('category/{id}', 'API\CategoryController@delete');
    
    // product route
    Route::get('products', 'API\ProductController@index');
    Route::post('products/create', 'API\ProductController@store');
    Route::get('products/{productId}', 'API\ProductController@show');
});

Route::get('airports', 'API\AirportController@index');

/* Route::group(['middleware' => 'cors'], function () {
    Route::get('airports', 'API\AirportController@index');
}); */

Route::get('airports', 'API\AirportController@index');